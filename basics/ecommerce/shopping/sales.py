# initialize module
print("Sales initialized.", __name__)

# this will not run if sales is imported as a module
if __name__ == "__main__":
    print("Sales started.")
    calc_tax()


def calc_tax():
    pass


def calc_shipping():
    pass
