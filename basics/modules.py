from ecommerce.shopping.sales import calc_shipping, calc_tax
from ecommerce.shopping import sales as sales  # alternative
from ecommerce.customer import contact
# from ..customer import contact  # relative import
from pprint import pprint
import sys

calc_shipping()
calc_tax()

sales.calc_shipping()
sales.calc_tax()

# pprint(sys.path, width=1)  # list of predefined python project paths

dir(sales)  # array of methods and attributes
sales.__name__  # ecommerce.shopping.sales
