from abc import ABC, abstractmethod
from collections import namedtuple


class Point:
    default_colour = "red"  # class level attribute (shared)

    # 'magic' methods
    def __init__(self, x, y):
        self.x = x  # instance level attribute (owned)
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"

    def __cmp__(self, other):
        return self.x == other.x and self.y == other.y

    def __gt__(self, other):
        return self.x > other.x and self.y > other.y

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    @classmethod  # decorator
    def zero(cls):  # factory method
        cls(0, 0)

    def draw(self):
        print(f"Point ({self.x}, {self.y})")


point = Point(1, 2)
isinstance(point, int)  # False

print(point.x)
point.draw()

another = Point(3, 4)
another.draw()

another.default_colour  # 'red' > instance level reference
Point.default_colour  # 'red' > class level reference

Point.default_colour = "yellow"
Point.default_colour  # 'yellow'
another.default_colour  # 'yellow'

point = Point.zero()
print(point)  # calls magic method '__str__'

point1 = Point(1, 2)
point2 = Point(1, 2)
point1 == point2  # False, compares memory address

point1 > point2  # Error

# custom container


class TagCloud:
    def __init__(self):
        self.__tags = {}  # private attribute (convention)

    def __getitem__(self, tag):
        return self.__tags.get(tag.lower(), 0)

    def __setitem__(self, tag, count):
        self.__tags[tag.lower()] = count

    def __len__(self):
        return len(self.__tags)

    def __iter__(self):
        return iter(self.__tags)  # returns iterator object

    def add(self, tag):
        self.__tags[tag.lower()] = self.__tags.get(tag.lower(), 0) + 1


cloud = TagCloud()
cloud.add("python")
cloud.add("python")
len(cloud)
cloud["python"] = 10
for tag in cloud:
    print(tag)

# {'__TagCloud__tags': {}} > private members are still accessible
print(cloud.__dict__)


class Product:
    def __init__(self, price):
        self.__price = price

    @property
    def price(self):
        return self.__price

    @price.setter
    def price(self, value):

        if value < 0:
            raise ValueError("Price cannot be negative.")
        else:
            self.__price = value


product = Product(10)
print(product.price)  # 10
# product.price = -1  # ValueError

# inheritance


class Animal:
    def __init__(self):
        self.age = 1

    def eat(self):
        print("eat")


class Mammal(Animal):
    def __init__(self):
        super().__init__()  # call parent constructor
        self.weight = 2

    def walk(self):
        print("walk")


class Fish(Animal):
    def swim(self):
        print("swim")


m = Mammal()
m.eat()  # inherited
m.walk()

m.age  # 1

isinstance(m, Mammal)  # True
isinstance(m, Animal)  # True
isinstance(m, object)  # True

o = object()  # empty object
issubclass(Mammal, Mammal)  # True
issubclass(Mammal, Animal)  # True
issubclass(Mammal, object)  # True

m.age  # 1
m.weight  # 2


class Employee:
    def greet(self):
        print("Employee Greet")


class Person:
    def greet(self):
        print("Person Greet")

# multiple inheritance called in order of declaration


class Manager(Employee, Person):
    pass


m = Manager()
m.greet()  # 'Employee Greet'


# stream of data

class InvalidOperationError(Exception):
    pass


class Stream(ABC):  # abstract class with methods
    def __init__(self):
        self.opened = False

    def open(self):
        if self.opened:
            raise InvalidOperationError("Stream is already opened.")
        self.opened = True

    def close(self):
        if self.opened:
            raise InvalidOperationError("Stream is already closed.")
        self.opened = False

    @abstractmethod
    def read(self):
        pass


class FileStream(Stream):
    def read(self):
        print("Reading data from a file.")


class NetworkStream(Stream):
    def read(self):
        print("Reading data from a network.")


class Text(str):
    def duplicate(self):
        return self + self


text = Text("Python")
text.duplicate()  # 'PythonPython'


class TrackableList(list):
    def append(self, object):
        print("Append called..")
        super().append(object)


# data class with attributes only - immutable
Value = namedtuple("Value", ["value"])
v1 = Value(value=1)
v2 = Value(value=1)

v1 == v2  # True
