from collections import deque
from array import array
from sys import getsizeof
from pprint import pprint
from functools import reduce

# lists (mutable)
["a", "b", 1, False]

[[1, 2], [3, 4]]  # matrix

# operations
[0] * 100  # create list of 100 0s
[1] + ["a"]  # [1, "a"] > concatination

# functions
list(range(1, 21))  # craete list of 1..20
list("Hello World")  # ["H", "e", "l", ...]
len([1, 2, 3, 4])  # 4

letters = ["a", "b", "c", "d"]
letters[0]  # 'a'
letters[-1]  # 'd'
letters[0:3]  # ["a", "b", "c"]
letters[0:]  # ["a", "b", "c", "d"]
letters[:]  # ["a", "b", "c", "d"]
letters[::2]  # ["a", "c"] > step

numbers = list(range(20))
numbers[::2]  # [0, 2, 4, 6, ...]
numbers[::-1]  # [19, 18, 17, ...]

# list unpacking
numbers = [1, 2, 3]
first, second, third = numbers  # number of variables must match list size.
first, second, *other = numbers  # first=1, second=2, other=[3]
first, *other, last = numbers  # first=1, other=[2], last=3

for index, letter in enumerate(letters):  # enumerate returns a tuple
    print(index, letter)

# methods
letters = ["a", "b", "c"]
letters.append("d")  # ["a", "b", "c", "d"]
letters.insert(0, "-")  # ["-", "a", "b", "c", "d"]
letters.pop()  # ["-", "a", "b", "c"]
letters.pop()  # ["a", "b", "c"]
letters.remove("b")  # ["a", "c"]
del letters[0]  # ["a"]
del letters[:]  # []
letters.clear()  # []

letters = ["a", "b", "c"]
letters.index("a")  # 0
# letters.index("d") # Error
"d" in letters  # False
letters.count("a")  # 1

unsorted = [3, 51, 2, 8, 6]
unsorted.sort()  # modifies original list
unsorted.sort(reverse=True)
sorted(unsorted)  # returns new list

# sort complex objects
# item: name, price
items = [
    ("Product 1", 10),
    ("Product 2", 5),
    ("Product 3", 12)
]


def sort_item(item):
    return item[1]


items.sort(key=sort_item)
items.sort(key=lambda item: item[1])

transformation = map(lambda item: item[1], items)  # returns a map object
mapped = list(transformation)
print(mapped)

# returns a filter object
transformation = filter(lambda item: item[1] >= 10, items)
filtered = list(transformation)
print(filtered)

# list comprehension
prices = list(map(lambda item: item[1], items))
prices = [item[1] for item in items]  # [expression for item in items]

filtered = list(filter(lambda item: item[1] >= 10, items))
# [expression for item in items if condition]
filtered = [item for item in items if item[1] >= 10]

list1 = [1, 2, 3]
list2 = [10, 20, 30]

transformation = zip(list1, list2)  # returns a zip function
zipped = list(transformation)  # [(1, 10), (2, 20), (3, 30)]

transformation = zip("abc", list1, list2)
zipped = list(transformation)  # [("a", 1, 10), ("b", 2, 20), ("c", 3, 30)]

# stacks, lifo - last in first out
browsing_session = []
browsing_session.append(1)  # [1]
browsing_session.append(2)  # [1, 2]
browsing_session.append(3)  # [1, 2, 3]
value = browsing_session.pop()  # value=3, [1, 2]
browsing_session[-1]  # 2

if not browsing_session:  # [] is Falsy
    print("Empty")

# queues, fifo - first in first out
queue = deque([])  # more performant than a list
queue.append(1)  # [1]
queue.append(2)  # [1, 2]
queue.append(3)  # [1, 2, 3]
queue.popleft()  # [2, 3]

# tuples
point = 1, 2  # (1, 2)
point = 1,  # (1) > trailing comma
point = ()  # ()

concatinate = (1, 2) + (3, 4)  # (1, 2, 3, 4)
repeate = (1, 2) * 3  # (1, 2, 1, 2, 1, 2)
point = tuple([1, 2])  # (1, 2)
point = tuple("ab")  # ("a", "b")

point = (1, 2, 3)
point[0:2]  # (1, 2)
x, y, z = point  # x=1, y=2, z=3

# point[0] = 10 # Error, tuples are immutable

# swap two variables
x = 10
y = 11

# naive
z = x
x = y
y = z

# python
x, y = y, x  # packing and unpacking tuple

# array vs list - array is more performant
# b -> signed char
# B -> unsigned char
# u -> py_unicode
# i -> signed int
# f -> float
# etc...
numbers = array("i", [1, 2, 3])
numbers.append(4)
numbers.insert(0, 0)
numbers.pop(0)
numbers.remove(4)
# numbers[0] = 1.0 # Error, incorrect type

# set - unordered collection of unique values
numbers = [1, 2, 2, 2, 3, 4, 5, 5]
uniques = set(numbers)  # {1, 2, 3, 4, 5}
second = {1, 4}
second.add(5)  # {1, 4, 5}
second.remove(5)  # {1, 4}
len(second)  # 2

first = {1, 2, 3, 4}
second = {1, 5}

first | second  # {1, 2, 3, 4, 5} > union - all items from either set
first & second  # {1} > intersection - only items in both sets
first - second  # {2, 3, 4} > difference - remove items from set
first ^ second  # {2, 3, 4, 5} symetric difference - first or second, not both

# dictionary
point = {"x": 1, "y": 2}
point = dict(x=1, y=2)  # {"x":1, "y":2}
point["x"]  # 1
point["x"] = 10
point["z"] = 20  # {"x":10, "y":2, "z":20}

# point["a"] = 4 # Error, no such key
"a" in point  # False

point.get("a", 0)  # 0 (default)
del point["x"]  # {"y":2, "z":20}

for k in point:
    print(k, point[k])

for k, v in point.items():
    print(k, v)

# list comprehension with lists, sets and dictionaries
values = [i * 2 for i in range(5)]  # [0, 2, 4, 6, 8] > list
values = {i * 2 for i in range(5)}  # {0, 2, 4, 6, 8} > set
values = {i: i * 2 for i in range(5)}  # {0:0, 1:2, 2:4, 3:6, 4:8}
values = (i * 2 for i in range(5))  # returns a generator object

# generators - for large or infinite data sets
values = (i * 2 for i in range(1000))
getsizeof(values)  # 120 (bytes)
values = (i * 2 for i in range(100000))
getsizeof(values)  # 120 (bytes)
# len(values) # Error, no len

# unpacking operator '*'
numbers = [1, 2, 3]
print(numbers)  # [1, 2, 3]
print(*numbers)  # 1 2 3

values = list(range(5))
print(values)  # [0, 1, 2, 3, 4]

values1 = [*range(5)]  # [0, 1, 2, 3, 4]
values2 = [*"Hello"]  # ["H", "e", "l", "l", "o"]
values3 = [*values1, "b", *values2, 4]

first = {"x": 1}
second = {"x": 10, "y": 2}
combined = {**first, **second}  # {"x": 10, "y": 2}

# Most repeated character


def findFrequentExample(sentence):
    frequency = {i: 0 for i in sentence}
    for char in sentence:
        frequency[char] += 1
    pprint(frequency, width=1)
    frequency_sorted = sorted(
        frequency.items(),
        key=lambda kv: kv[1],
        reverse=True)
    return frequency_sorted[0]


def findFrequent(sentence):
    frequency = {i: 0 for i in sentence}
    for c in sentence:
        frequency[c] += 1
    return reduce(lambda x, y: x if x[1] > y[1] else y, frequency.items())


sequence = "aaababbccdaacbbbb"
print("Exercise")
print(f"Sequence: {sequence}")
print("Most repeated character:", findFrequent(sequence))
