from pathlib import Path
from zipfile import ZipFile
from time import ctime
from datetime import datetime, timedelta
from string import Template
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import time
import sqlite3
import shutil
import json
import csv
import sys
import random
import string
import webbrowser
import smtplib
import subprocess

Path(r"C:\Program Files\Microsoft")  # raw string
Path("/usr/local/bin")
Path()  # current folder
Path("ecommerce/__init__.py")
Path() / Path("ecommerce") / "__init__.py"  # concatinate paths
Path().home

path = Path("ecommerce/__init__.py")
print(path.exists())
print(path.is_file())
print(path.is_dir())
print(path.name)
print(path.stem)
print(path.suffix)
print(path.parent)

path = path.with_name("file.py")
path = path.with_suffix(".txt")
print(path.absolute())

# path.exists()
# path.mkdir()
# path.rmdir()
# path.rename("ecommerce2")

path = Path("ecommerce")
path.iterdir()  # list of files and directories in path > returns a generator
for p in path.iterdir():
    print(p)

paths = [p for p in path.iterdir() if p.is_dir()]
py_files = [p for p in path.glob("*.py")]  # returns a generator
# recursive glob > returns a generator
all_py_files = [p for p in path.rglob("*.py")]
print(paths)
print(py_files)
print(all_py_files)

print(ctime(path.stat().st_ctime))  # convert epoch to date time
# path.read_text()  # reads all bytes, handles stream open and close
# path.write_text("...")
# path.write_bytes()

# copy file
# source = Path("ecommerce/__init__.py")
# target = Path() / "__init__.py"
# shutil.copy(source, target)

# zip
with ZipFile("files.zip", "w") as zip:
    for path in Path("ecommerce").rglob("*.*"):
        zip.write(path)

with ZipFile("files.zip") as zip:
    print(zip.namelist())

# csv
with open("data.csv", "w") as file:
    writer = csv.writer(file)
    writer.writerow(["transaction_id", "product_id", "price"])
    writer.writerow([1000, 1, 5])
    writer.writerow([1000, 2, 15])

with open("data.csv", "r") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row)

# json
movies = [
    {"id": 1, "title": "Terminator", "year": 1984},
    {"id": 2, "title": "Kindergarten Cop", "year": 1990}
]

data = json.dumps(movies)
Path("movies.json").write_text(data)

data = Path("movies.json").read_text()
movies = json.loads(data)
print(movies[0]["title"])

# sqlite3
movies = json.loads(Path("movies.json").read_text())
print(movies)

with sqlite3.connect("db.sqlite3") as conn:  # creates if doesn't exist
    command = "INSERT INTO Movies VALUES(?, ?, ?)"
    for movie in movies:
        conn.execute(command, tuple(movie.values()))

with sqlite3.connect("db.sqlite3") as conn:
    command = "SELECT * FROM Movies"
    cursor = conn.execute(command)
    for row in cursor:
        print(row)
    # movies = cursor.fetchall() # list of all rows
    # print(movies)


def send_emails():
    for i in range(10000):
        pass


start = time.time()
send_emails()
end = time.time()
duration = end - start
print(duration)

# datetime
dt = datetime(2018, 1, 1)
dt1 = datetime.now()
dt2 = datetime.strptime("2018/01/01", "%Y/%m/%d")
dt3 = datetime.fromtimestamp(time.time())

print(f"{dt.year}/{dt.month}")
dt.strftime("%Y/%m")  # 2019/08

duration = dt1 - dt2
print("days", duration.days)
print("seconds", duration.seconds)
print("total seconds", duration.total_seconds())

print(random.random())  # float
print(random.randint(1, 10))  # int
print(random.choice([1, 2, 3, 4]))
print(random.choices([1, 2, 3, 4], k=2))

print(string.ascii_letters)
print(string.digits)

# password generator
print("".join(random.choices(string.ascii_letters + string.digits, k=10)))
numbers = [1, 2, 3, 4]
random.shuffle(numbers)

# open browser
print("Deployment completed")
webbrowser.open("http://google.com")

template = Template(Path("template.html").read_text())

# send email
message = MIMEMultipart()
message["from"] = "Sean Midford"
message["to"] = "sean.midford.tutoring@gmail.com"
message["subject"] = "This is a test"
body = template.substitute({"name": "Sean"})
message.attach(MIMEText(body, "html"))
message.attach(MIMEImage(Path("mosh.png").read_bytes()))

with smtplib.SMTP(host="smtp.gmail.com", port=587) as smtp:
    smtp.ehlo()
    smtp.starttls()
    smtp.login("testuser@gmail.com", "password1234")
    smtp.send_message(message)
    print("Sent...")

# command line arguments
print(sys.argv)

if len(sys.argv) == 1:
    print("USAGE: python app.py <password>")
else:
    print("Password:", sys.argv[1])

# running on the command line
# subprocess.call  # legacy
completed = subprocess.run(["ls", "-l"], capture_output=True)
print("args", completed.args)
print("returncode", completed.returncode)
print("stderr", completed.stderr)
print("stdout", completed.stdout)

# run child process
try:
    completed = subprocess.run(["python3", "other.py"],
                               capture_output=True, text=True)
except subprocess.CalledProcessError as ex:
    print(ex)
