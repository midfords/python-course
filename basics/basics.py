import math

# printing
print("Hello World!")
print("*" * 10)

# labels / variables
students_count = 1000  # int
print(students_count)

rating = 4.99  # float
print(rating)

is_published = False  # boolean (capitalized)
print(is_published)

course_name = "Python Programming"  # string
print(course_name)

# strings
course = "Python Programming"
len(course)  # 18
course[0]  # 'P'
course[-1]  # 'g'

# slicing
course[0:3]  # 'Pyt'
course[0:]  # 'Python Programming'
course[:3]  # 'Pyt'
course[:]  # 'Python Programming'

escape = "Escaped \" \' \\ \n characters"

# formatted string
first = "Sean"
last = "Midford"
full = f"{first} {last}"
print(full)

# string methods
course = " python "
course.upper()  # ' PYTHON '
course.lower()  # ' python '
course.title()  # ' Python '
course.strip()  # 'python'
course.find("pyt")  # 1 > index of (-1 for not found)
course.replace("py", "pi")  # ' pithon '
"py" in course  # True
"swift" not in course  # True

# numbers
x = 1
x = 1.1
x = 1 + 2j  # complex number (j > i)

# operations
10 + 3
10 - 3
10 * 3
10 / 3  # with decimal
10 // 3  # truncate
10 % 3  # remainder (mod)
10 ** 3  # power

x = 10
x += 3  # Augmented assignment operator

round(2.9)  # 3
abs(-2)  # 2

# math module
math.ceil(3.3)  # 4

x = input("x: ")  # always gives a string

int(x)
float(x)
bool(x)
str(x)
type(x)  # class 'str'

# Falsy
""
0
None

# Truthy (everything else)

# comparison operators (logic)
10 > 3
10 >= 3
10 < 20
10 <= 20
10 == 10
10 == "10"  # False
10 != "10"  # True

"bag" > "apple"  # True (alphabetically 'bag' comes last)
"bag" == "BAG"  # False
ord("b")  # 98

# if statements
temperature = 35
if temperature > 30:
    print("Hot hot hot!")
elif temperature > 20:
    print("Not too bad.")
else:
    print("Kind of cool")

# ternary operator
age = 22
message = "Eligible" if age >= 18 else "Not eligible"
print(message)

# and or not
True and True  # True
False and True  # False
not False or False  # True

# for loops
for number in range(3):
    print("Looping")

for number in range(1, 4):
    print("Loop ", number, number * ".")

for number in range(1, 10, 2):
    print("Loop ", number, number * ".")


# for - else (no break)
successful = False
for number in range(3):
    print("Attempt")
    if successful:
        print("Successful")
        break
else:
    print("Failed")

# nesting
for x in range(3):
    for y in range(3):
        print(f"({x}, {y})")

# iterables
type(5)  # class 'int'
type(range(5))  # class 'range' (complex type)

for x in range(5):
    print("iterable")

for c in "Python":
    print(c)

for i in [1, 2, 3, 4]:
    print(i)

# while loops
number = 100
while number > 0:
    print(number)
    number //= 2

cmd = ""
while cmd.lower() != "quit":
    cmd = input("> ")
    print("echo", cmd)

while True:
    cmd = input("> ")
    print("echo", cmd)
    if cmd.lower() == "quit":
        break
