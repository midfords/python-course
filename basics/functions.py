def greet(first, last):
    print(f"Hello, {first} {last}!")


greet("Sean", "Midford")

# parameter - name for a variable passed to function
# argument - actually value being passed

# two types of functions:
# 1- perform a task
# 2- return a value


def get_greeting(name):
    return f"Hello, {name}"


greeting = get_greeting("Sean")
print(greeting)

file = open("out.txt", "w")
file.write(greeting)

val = greet("Sean", "Midford")  # None


def increment(number, by=1):  # optional parameter (must come after required params)
    return number + by


result = increment(10, by=2)  # keyword argument
print(result)


def multiply(*numbers):  # xargs, supplies a tuple (immutable)
    total = 1
    for number in numbers:
        number *= number
    return number


result = multiply(2, 3, 4, 5)
print(result)


def save_user(**user):  # xxargs, supplies dictionary of keyword arguments
    print(user["name"])


save_user(id=1, name="Sean", age=22)

# scope
scope = "a"


def func():
    # global scope = "b" # modifies global variable, bad practice
    scope = "b"


func()
print(scope)  # 'a'
