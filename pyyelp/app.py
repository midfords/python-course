import requests
import config

url = "https://api.yelp.com/v3/businesses/search"
api_key = config.api_key

headers = {
    "Authorization": "Bearer " + api_key
}
parameters = {
    "location": "Ottawa"
}
response = requests.get(
    url,
    headers=headers,
    params=parameters
)
businesses = response.json()['businesses']
names = [business["name"]
         for business in businesses if business["rating"] > 4.0]
print(names)
