from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from .models import Movie

# Create your views here.


def index(request):
    # SELECT * FROM movies_movie
    movies = Movie.objects.all()

    # SELECT * FROM movies_movie WHERE ...
    # Movie.objects.filter(release_year=1984)
    # Movie.objects.get(id=1)

    return render(request, 'movies/index.html', {'movies': movies})


def details(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)
    return render(request, 'movies/details.html', {'movie': movie})
